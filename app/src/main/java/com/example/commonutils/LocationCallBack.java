package com.example.commonutils;

/**
 * Created by pawars on 12/3/2017.
 */

public interface LocationCallBack {
    void locationReceived(String latitude, String longitude, String currentTime);
}
