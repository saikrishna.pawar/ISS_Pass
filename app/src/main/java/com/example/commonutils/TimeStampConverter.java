package com.example.commonutils;

import java.util.Date;

/**
 * Created by pawars on 12/3/2017.
 */

public class TimeStampConverter {

    private static TimeStampConverter mTimeStampConverter;

    private TimeStampConverter() {

    }

    public static TimeStampConverter getInstance() {
        if (mTimeStampConverter == null) {
            mTimeStampConverter = new TimeStampConverter();
        }
        return mTimeStampConverter;
    }

    /**
     * Returns the date from unix time stamp
     *
     * @return
     */
    public Date getDateFromUnixTimeStamp(Integer timeStamp) {
        Date time = new Date((long) timeStamp * 1000);
        return time;
    }
}
